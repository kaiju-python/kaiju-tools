- feat: rewrite encoders to msgspec
- fix: stream test fixed

<br>

### 2.2.4


<br>

### 2.2.3

- fix: rpc.routes for disable_permission settings
- fix: on exit timeout for scheduler tasks

<br>

### 2.2.2

- fix: tcp connector init fix for aiohttp

<br>

### 2.2.1

- fix: fixed test fixtures for app

<br>

### 2.2.0

- fix: moved services initial init to init_app

<br>

### 2.1.30


<br>

### 2.1.27

- fix: annotation parser multifix

<br>

### 2.1.26

- fix: HTTPService ret None for empty json body if a

<br>

### 2.1.25

- feat: tcp_connector_settings arg for HTTPService i
- feat: tcp_connector_settings arg for HTTPService i

<br>

### 2.1.24

- fix: queue logger handler error handling

<br>

### 2.1.23

- feat: arbitrary data field in REQUEST_CONTEXT

<br>

### 2.1.22

