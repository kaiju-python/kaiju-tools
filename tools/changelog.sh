#!/usr/bin/env sh

# Print changelog of the last version features and bug fixes
# sed '/###/q' CHANGES.rst - last changes

set -e

. ./tools/_lib.sh

print_changelog
