#!/usr/bin/env python3

import os
from json import dumps
from urllib.error import HTTPError
from urllib.request import Request, urlopen


PROJECT = 'kaiju-tools'
BRANCH = 'rev-2'
URL = 'https://api.telegram.org/bot{token}/sendMessage'
NOTIFICATION_TEMPLATE = './etc/notification.html'
TEMPLATE_TYPE = 'HTML'
CHANGELOG = 'CHANGES.md'
VERSION_FILE = '.bumpversion.cfg'
PROJECT_URL = f'https://gitlab.com/kaiju-python/{PROJECT}'


def get_package_version() -> str:
    with open(VERSION_FILE) as f:
        for line in f:
            if line.startswith('current_version'):
                return line.replace('current_version =', '', 1).strip()


def parse_changelog(path: str):
    version, commits = None, []
    version = get_package_version()
    with open(path) as f:
        for line in f:
            if line.startswith('-'):
                line = line.replace('-', '', 1).strip()
                commits.append([s.strip() for s in line.split(':')])
            elif line.startswith('<br>'):
                break
    return version, commits


def create_notification(version: str, commits: list) -> str:
    message = f'<b>{PROJECT}</b> {version}\n\n'
    for prefix, commit in commits:
        message += f'- {prefix}: {commit}\n'
    message += f'\n<pre>pip install -U {PROJECT}=={version}</pre>\n'
    message += f'<a href="{PROJECT_URL}/-/blob/{BRANCH}/{CHANGELOG}">changelog</a>\n'
    return message


def send_changelog_notification(channel_id: str, token: str):
    url = URL.format(token=token)
    version, commits = parse_changelog(CHANGELOG)
    notification = create_notification(version, commits)
    data = {'chat_id': channel_id, 'parse_mode': TEMPLATE_TYPE, 'text': notification}
    data = dumps(data).encode('utf-8')
    request = Request(url, method='POST', headers={'Content-Type': 'application/json'}, data=data)
    try:
        with urlopen(request):
            return
    except HTTPError as exc:
        print(exc.read())


if __name__ == '__main__':
    _ch_id = os.getenv('TG_CHANNEL')
    _token = os.getenv('TG_BOT_TOKEN')
    assert _token is not None and _ch_id is not None, '$TG_CHANNEL and $TG_BOT_TOKEN env vars must be set'
    send_changelog_notification(_ch_id, _token)
