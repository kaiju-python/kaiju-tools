
.. image:: https://badge.fury.io/py/kaiju-tools.svg
    :target: https://pypi.org/project/kaiju-tools
    :alt: Latest package version

.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
   :target: https://github.com/psf/black
   :alt: Code style - Black

`Python <https://www.python.org>`_ >=3.11

Core services and classes for web RPC server.
See `documentation <https://kaiju-tools.readthedocs.io/>`_ for more info.

Installation
------------

pip install kaiju-tools
