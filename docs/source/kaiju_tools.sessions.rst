**sessions** - request session management
=========================================

.. automodule:: kaiju_tools.sessions
   :members:
   :undoc-members:
   :show-inheritance:
