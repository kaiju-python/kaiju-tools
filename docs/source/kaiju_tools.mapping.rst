**mapping** - dictionary functions
==================================

This module contains a number of useful dictionary functions.

.. automodule:: kaiju_tools.mapping
   :members:
   :undoc-members:
   :show-inheritance:
