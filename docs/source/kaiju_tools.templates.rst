**templates** - dynamic data generation
=======================================

This module contains tools for creating conditions and templates to dynamically generate and validate
python data structures (lists and dicts).

See :ref:`templates-howto` for more info on template syntax and use cases.

.. autoclass:: kaiju_tools.templates.Template
   :members:
   :show-inheritance:
   :exclude-members: Definitions

.. autoclass:: kaiju_tools.templates.Condition
   :members:
   :show-inheritance:
   :exclude-members: Definitions

.. autodata:: kaiju_tools.templates.COMPARISON_FUNCTIONS
   :no-value:

.. literalinclude:: ../../kaiju_tools/templates.py
   :start-after: >> comparison_functions
   :end-before: << comparison_functions

.. autodata:: kaiju_tools.templates.AGG_FUNCTIONS
   :no-value:

.. literalinclude:: ../../kaiju_tools/templates.py
   :start-after: >> agg_functions
   :end-before: << agg_functions

.. autodata:: kaiju_tools.templates.TEMPLATE_FUNCTIONS
   :no-value:

.. literalinclude:: ../../kaiju_tools/templates.py
   :start-after: >> template_functions
   :end-before: << template_functions
