**functions** - useful functions
================================

Various useful functions and decorators.

.. autofunction:: kaiju_tools.functions.retry

.. autofunction:: kaiju_tools.functions.retry_

.. autoclass:: kaiju_tools.functions.RetryException
   :show-inheritance:
   :exclude-members: __init__

.. autofunction:: kaiju_tools.functions.get_short_uid

.. autofunction:: kaiju_tools.functions.secure_uuid

.. autofunction::  kaiju_tools.functions.timeout

.. autofunction::  kaiju_tools.functions.not_implemented

.. autofunction:: kaiju_tools.functions.async_run_in_thread

.. autofunction::  kaiju_tools.functions.async_
