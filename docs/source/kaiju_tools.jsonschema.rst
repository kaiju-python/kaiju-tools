.. _jsonschema-api:

**jsonschema** - python object validation
=========================================

This module implements most of `jsonschema <https://json-schema.org/understanding-json-schema/>`_ draft 7
specification for validating python json-like objects (dictionaries, lists and other simple data types).

You can use python classes to describe a validator schema and compile this schema into a validation function which
accepts a json-like object and returns its validated and normalized copy.

.. code-block:: python

    import jsonschema as j

    obj = j.Object({
        'my_string': j.String(minLength=1, default='default_string')
    }, required=['my_string'], additionalProperties=False)

    validator = j.compile_schema(obj)
    obj = validator({'my_string': 'abc'})  # returns a new validated and normalized object

.. autofunction:: kaiju_tools.jsonschema.compile_schema

.. autoclass:: kaiju_tools.jsonschema.JSONSchemaObject
   :members: __init__
   :show-inheritance:

.. autoclass:: kaiju_tools.jsonschema.Constant
   :members: __init__
   :show-inheritance:

.. autoclass:: kaiju_tools.jsonschema.Boolean
   :members: __init__
   :show-inheritance:

.. autoclass:: kaiju_tools.jsonschema.String
   :members: __init__
   :show-inheritance:

.. autoclass:: kaiju_tools.jsonschema.GUID
   :show-inheritance:

.. autoclass:: kaiju_tools.jsonschema.Date
   :show-inheritance:

.. autoclass:: kaiju_tools.jsonschema.DateTime
   :show-inheritance:

.. autoclass:: kaiju_tools.jsonschema.Number
   :members: __init__
   :show-inheritance:

.. autoclass:: kaiju_tools.jsonschema.Integer
   :members: __init__
   :exclude-members: include_null_values, repr, serializable_attrs
   :show-inheritance:
   :inherited-members:

.. autoclass:: kaiju_tools.jsonschema.Object
   :members: __init__
   :show-inheritance:

.. autoclass:: kaiju_tools.jsonschema.Null
   :exclude-members: __init__
   :show-inheritance:

.. autoclass:: kaiju_tools.jsonschema.Array
   :members: __init__
   :show-inheritance:

.. autoclass:: kaiju_tools.jsonschema.AnyOf
   :members: __init__
   :show-inheritance:
   :inherited-members:
   :exclude-members: include_null_values, repr, serializable_attrs

.. autoclass:: kaiju_tools.jsonschema.AllOf
   :members: __init__
   :show-inheritance:
   :inherited-members:
   :exclude-members: include_null_values, repr, serializable_attrs

.. autoclass:: kaiju_tools.jsonschema.OneOf
   :members: __init__
   :show-inheritance:
   :inherited-members:
   :exclude-members: include_null_values, repr, serializable_attrs

.. autoclass:: kaiju_tools.jsonschema.Not
   :members: __init__
   :show-inheritance:
