**logging** - loggers and formatters
====================================

This module contains list of default log formatter and loggers used by package logging services.

.. autodata::  kaiju_tools.logging.FORMATTERS
   :no-value:

.. autodata::  kaiju_tools.logging.HANDLERS
   :no-value:

.. autoclass:: kaiju_tools.logging.Logger
   :members:

.. autoclass:: kaiju_tools.logging.TextFormatter
   :members:

.. autoclass:: kaiju_tools.logging.DataFormatter
   :members:

.. autoclass:: kaiju_tools.logging.JSONHandler
   :members:

.. autoclass:: kaiju_tools.logging.TextHandler
   :members:
