**cache** - base cache service
==============================

This module contains base cache service class. All actual implementations should in general use this class as a base.

.. autoclass:: kaiju_tools.cache.BaseCacheService
   :members:
   :exclude-members: CONNECTION_ERROR_CLASSES, init, close, get_transport_cls
   :show-inheritance:
