**rpc** - server and protocol
=============================

RPC classes and services.

Check :ref:`rpc-guide` and :ref:`rpc-services-howto` on how to use and implement RPC services.

.. autodata:: kaiju_tools.rpc.JSONRPC

.. autofunction:: kaiju_tools.rpc.debug_only

.. autoclass:: kaiju_tools.rpc.JSONRPCServer
   :members:
   :show-inheritance:
   :exclude-members: init, close, closed, routes, permissions

.. autoclass:: kaiju_tools.rpc.BaseRPCClient
   :members:
   :show-inheritance:
   :exclude-members: init, close, closed, Topic

.. autoclass:: kaiju_tools.rpc.RPCRequest
   :members:
   :undoc-members:

.. autoclass:: kaiju_tools.rpc.RPCResponse
   :members:
   :undoc-members:

.. autoclass:: kaiju_tools.rpc.RPCError
   :members:
   :undoc-members:

.. autoclass:: kaiju_tools.rpc.JSONRPCHeaders
   :members:
   :undoc-members:

.. autoclass:: kaiju_tools.rpc.PermissionKeys
   :members:
   :undoc-members:

.. autoclass:: kaiju_tools.rpc.RPCClientError
   :members:
   :show-inheritance:
