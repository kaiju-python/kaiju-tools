**exceptions** - standard error classes
=======================================

Use these error types if you want to propagate errors across your apps.

If you want to create your own error class you should inherit it from :py:class:`~kaiju_tools.exceptions.ClientError`
or :py:class:`~kaiju_tools.exceptions.InternalError` depending on what type of exception it is and then you should
register it in :py:obj:`~kaiju_tools.exceptions.ERROR_CLASSES` registry so the RPC client will be able to reconstruct
the exception from a json response.

.. code-block:: python

  from kaiju_tools.exceptions import ClientError, ERROR_CLASSES

  class ClientIsBanned(ClientError):
    """Client account is banned."""

  ERROR_CLASSES.register(ClientError)

The difference between :py:class:`~kaiju_tools.exceptions.ClientError`
and :py:class:`~kaiju_tools.exceptions.InternalError` is that the former is not considered an error from the application
perspective, i.e. it will not produce an error log and trace records.

:py:class:`~kaiju_tools.exceptions.ClientError`

- Does not print stack trace
- Has 'INFO' log level
- All error data is sent to the client

:py:class:`~kaiju_tools.exceptions.InternalError`

- May print stack trace
- Has 'ERROR' log level
- Error data is not sent to the client

Choose between these two base depending on your error purpose. If you raise an error to signal a client that something
is wrong with the client input (validation, object not found or can't be accessed, etc) you should use `ClientError`.
For server errors use `InternalError`.

.. autodata:: kaiju_tools.exceptions.ERROR_CLASSES
   :no-value:

.. autoclass:: kaiju_tools.exceptions.APIException
   :members: status_code, __init__
   :show-inheritance:

.. autoclass:: kaiju_tools.exceptions.ClientError
   :members: status_code, __init__
   :show-inheritance:

.. autoclass:: kaiju_tools.exceptions.JSONParseError
   :members: status_code, __init__
   :show-inheritance:

.. autoclass:: kaiju_tools.exceptions.InvalidRequest
   :members: status_code, __init__
   :show-inheritance:

.. autoclass:: kaiju_tools.exceptions.MethodNotFound
   :members: status_code, __init__
   :show-inheritance:

.. autoclass:: kaiju_tools.exceptions.InvalidParams
   :members: status_code, __init__
   :show-inheritance:

.. autoclass:: kaiju_tools.exceptions.RequestTimeout
   :members: status_code, __init__
   :show-inheritance:

.. autoclass:: kaiju_tools.exceptions.Aborted
   :members: status_code, __init__
   :show-inheritance:

.. autoclass:: kaiju_tools.exceptions.InternalError
   :members: status_code, __init__
   :show-inheritance:

.. autoclass:: kaiju_tools.exceptions.HTTPRequestError
   :members: status_code, __init__
   :show-inheritance:

.. autoclass:: kaiju_tools.exceptions.NotAuthorized
   :members: status_code, __init__
   :show-inheritance:

.. autoclass:: kaiju_tools.exceptions.NotFound
   :members: status_code, __init__
   :show-inheritance:

.. autoclass:: kaiju_tools.exceptions.Conflict
   :members: status_code, __init__
   :show-inheritance:

.. autoclass:: kaiju_tools.exceptions.FailedDependency
   :members: status_code, __init__
   :show-inheritance:

.. autoclass:: kaiju_tools.exceptions.InvalidLicense
   :members: status_code, __init__
   :show-inheritance:
