**encoding** - data serialization
=================================

You can use encoding classes for loading and storing data in external systems. Currently supported formats are listed
in :py:class:`~kaiju_tools.encoding.MimeTypes`.

.. code-block:: python

  from kaiju_tools.encoding import SERIALIZERS

  def my_loading_function(data, mime_type):
    encoder = SERIALIZERS(mime_type)()
    return encoder.loads(data)

You can also use :py:class:`~kaiju_tools.encoding.Serializable` to create serializable objects accepted by encoders
and implement custom serialization logic in its :py:meth:`~kaiju_tools.encoding.Serializable.repr` method.

.. code-block:: python

  from kaiju_tools.encoding import Serializable

  class MySerializableClass(Serializable):

    def __init__(self, a, b):
      self.a = a
      self.b = b

    def repr(self):
      return {'a': self.a, 'b': self.b, 'c': 42}

.. autofunction:: kaiju_tools.encoding.loads

.. autofunction:: kaiju_tools.encoding.dumps

.. autoclass:: kaiju_tools.encoding.MimeTypes
   :members:
   :undoc-members:

.. autodata:: kaiju_tools.encoding.SERIALIZERS
   :no-value:

.. autodata:: kaiju_tools.encoding.MSGPACK_TYPES
   :no-value:

.. autoclass:: kaiju_tools.encoding.Serializable
   :members:

.. autoclass:: kaiju_tools.encoding.MsgpackType
   :members:

.. autoclass:: kaiju_tools.encoding.JSONSerializer
   :members:

.. autoclass:: kaiju_tools.encoding.MsgpackSerializer
   :members:

.. autoclass:: kaiju_tools.encoding.SerializerInterface
   :members:
