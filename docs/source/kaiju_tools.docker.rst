**docker** - container and stack management
===========================================

.. automodule:: kaiju_tools.docker
   :members:
   :undoc-members:
   :show-inheritance:
