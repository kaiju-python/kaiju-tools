.. include:: readme.rst

Guides
------

.. toctree::
   :maxdepth: 1

   app_guide
   rpc_guide
   templates_howto
   dev_guide

Library Reference
-----------------

.. toctree::
   :maxdepth: 1

   kaiju_tools.app
   kaiju_tools.cache
   kaiju_tools.docker
   kaiju_tools.encoding
   kaiju_tools.exceptions
   kaiju_tools.functions
   kaiju_tools.http
   kaiju_tools.jsonschema
   kaiju_tools.interfaces
   kaiju_tools.locks
   kaiju_tools.logging
   kaiju_tools.mapping
   kaiju_tools.registry
   kaiju_tools.rpc
   kaiju_tools.sessions
   kaiju_tools.streams
   kaiju_tools.templates
   kaiju_tools.types

.. include:: license.rst
