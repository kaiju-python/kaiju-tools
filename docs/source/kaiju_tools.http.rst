**http** - http client and views
================================

.. autoclass:: kaiju_tools.http.HTTPService
   :members:
   :show-inheritance:
   :exclude-members: init, close, closed, resolve

.. autoclass:: kaiju_tools.http.RPCClientService
   :members:
   :show-inheritance:
   :inherited-members:
   :exclude-members: init, close, closed, resolve, service_name, Topic, discover_service

.. autoclass:: kaiju_tools.http.JSONRPCView
   :members:
   :show-inheritance:
