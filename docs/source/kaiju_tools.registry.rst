**registry** - python object management
=======================================

You can use these classes to manage collections of python objects or classes. Registries provide a unified interface
to validate, store, search and filter python objects and classes.

For class registry in general you need to specify a list of accepted base classes by defining
:py:meth:`~kaiju_tools.registry.ClassRegistry.get_base_classes` which is required. You can also want to change
default registry key function :py:meth:`~kaiju_tools.registry.ClassRegistry.get_key` which uses class `__name__`
by default.

.. code-block:: python

  import abc
  from typing import Type

  class BaseConverter(abc.ABC):
    data_type: str

    @abc.abstractmethod
    def convert(self, data):
      ...

  class ConvertersRegistry(ClassRegistry[str, Type[BaseConverter]]):

    @classmethod
    def get_base_classes(cls):
      return (BaseConverter,)

    def get_key(self, obj):
      return obj.data_type

  CONVERTERS = ConvertersRegistry()

Then you can register new classes anywhere in your code.

.. code-block:: python

  class XMLConverter(BaseConverter):
    data_type = 'xml'

    def convert(self, data):
      ...

  CONVERTERS.register(XMLConverter)

You can now dynamically load your classes in your code.

.. code-block:: python

  def convert_data(data, data_type):
    if data_type in CONVERTERS:
      conv = CONVERTERS[data_type]()
      return conv.convert(data)
    else:
      raise NotFound('Unsupported data type.')

.. autoclass:: kaiju_tools.registry.ClassRegistry
   :inherited-members:
   :members:
   :show-inheritance:

.. autoclass:: kaiju_tools.registry.ObjectRegistry
   :members:
   :show-inheritance:
   :inherited-members:

.. autoclass:: kaiju_tools.registry.FunctionRegistry
   :members:
   :show-inheritance:
   :inherited-members:

.. autoclass:: kaiju_tools.registry.RegistryError
   :show-inheritance:
   :exclude-members: __init__, __new__

.. autoclass:: kaiju_tools.registry.RegistrationFailed
   :show-inheritance:
   :exclude-members: __init__, __new__
