**locks** - base shared locks class
===================================

A base class for shared locks services. You can use shared locks services to share locks between instances of the app
or between multiple apps (via. Redis etc.). Shared locks service also manages namespaces and locks renewal.

.. autoclass:: kaiju_tools.locks.BaseLocksService
   :members:
   :exclude-members: ErrorCode, init, close, get_transport_cls
   :show-inheritance:
