.. _app-api:

**app** - base application classes
==================================

.. autofunction:: kaiju_tools.app.init_app

.. autofunction:: kaiju_tools.app.run_server

.. autodata:: kaiju_tools.app.SERVICE_CLASS_REGISTRY

.. autodata:: kaiju_tools.app.COMMANDS

.. autoclass:: kaiju_tools.app.ServiceClassRegistry
   :show-inheritance:

.. autoclass:: kaiju_tools.app.Commands
   :show-inheritance:

.. autoclass:: kaiju_tools.app.Service
   :members:
   :show-inheritance:

.. autoclass:: kaiju_tools.app.ContextableService
   :members:
   :show-inheritance:
   :inherited-members:

.. autoclass:: kaiju_tools.app.ServiceConfig
   :members:

.. autoclass:: kaiju_tools.app.ProjectSettings
   :members:

.. autoclass:: kaiju_tools.app.ConfigLoader
   :members:
   :show-inheritance:

.. autoclass:: kaiju_tools.app.ServiceContextManager
   :members:
   :show-inheritance:
   :exclude-members: init, close, items

.. autoclass:: kaiju_tools.app.BaseCommand
   :members:
   :show-inheritance:
   :exclude-members: init, close, closed

.. autoclass:: kaiju_tools.app.HandlerSettings
   :members:
   :undoc-members:

.. autoclass:: kaiju_tools.app.LoggerSettings
   :members:
   :undoc-members:

.. autoclass:: kaiju_tools.app.LoggingService
   :show-inheritance:

.. autoclass:: kaiju_tools.app.Scheduler
   :members:
   :show-inheritance:
   :exclude-members: init, close, closed, routes, ExecPolicy, list_tasks

.. autoclass:: kaiju_tools.app.ScheduledTask
   :members:

.. autoclass:: kaiju_tools.app.ExecPolicy
   :members:
   :undoc-members:
