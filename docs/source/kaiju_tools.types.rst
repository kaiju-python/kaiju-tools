**types** - basic types
=======================

This module contains a collection of various basic types and classes used throughout the package.

.. autoclass:: kaiju_tools.types.SortedStack
   :members:
   :show-inheritance:

.. autoclass:: kaiju_tools.types.RestrictedDict
   :show-inheritance:
   :exclude-members: __init__, __new__

.. autoclass:: kaiju_tools.types.Namespace
   :members:
   :show-inheritance:

.. autoclass:: kaiju_tools.types.RequestContext
   :members:
   :exclude-members: __init__, __new__
   :show-inheritance:

.. autoclass:: kaiju_tools.types.Session
   :members:
   :exclude-members: repr
   :show-inheritance:

.. autoclass:: kaiju_tools.types.Scope
   :members:
